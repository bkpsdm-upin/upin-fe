/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "components/views/Dashboard";
import TableList from "components/views/TableList.js";
import Typography from "components/views/Typography.js";
import Icons from "components/views/Icons.js";
import Maps from "components/views/Maps.js";
import Notifications from "components/views/Notifications.js";
import DataHukdis from "components/views/DataHukdis";
import KasusList from "components/views/kasus/KasusList";
import AjuanKasus from "components/views/kasus/AjuanKasus";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-35",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/ajuankasus",
    name: "Pengajuan Kasus",
    icon: "nc-icon nc-notes",
    component: AjuanKasus,
    layout: "/admin"
  },
  {
    path: "/kasus",
    name: "Data Kasus",
    icon: "nc-icon nc-paper-2",
    component: KasusList,
    layout: "/admin"
  },
  {
    path: "/hukdis",
    name: "Hukuman Disiplin",
    icon: "nc-icon nc-pin-3",
    component: DataHukdis,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Verifikasi",
    icon: "nc-icon nc-atom",
    component: Icons,
    layout: "/admin"
  },  
  {
    path: "/notifications",
    name: "Rekapitulasi",
    icon: "nc-icon nc-bell-55",
    component: Notifications,
    layout: "/admin"
  }
];

export default dashboardRoutes;
