import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import accountReducer from './accountReducer';
import errorReducer from './errorReducer';
import menuReducer from './menuReducer';

export default combineReducers({
    account: accountReducer,
    form: formReducer,
    error: errorReducer,
    menu: menuReducer
})