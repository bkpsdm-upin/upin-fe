import {
    SIGN_IN,
    CEK_COOKIES,
    SIGN_OUT
} from '../action/types';

export default (state = {}, action) => {
    
    switch(action.type){
        case SIGN_IN:
            return action.payload;
        case CEK_COOKIES:
            return action.payload;
        case SIGN_OUT:
            return state;
        default:
            return state;
    }
}