import { CONFIG_LOGIN } from "./configLogin";
import { CONFIG_ENDPOINT } from "./configEndpoint";
export const API_LOGIN = CONFIG_LOGIN;
export const END_POINT = CONFIG_ENDPOINT;
