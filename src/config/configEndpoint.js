export const CONFIG_ENDPOINT = {
    // HOST : 'http://localhost:8090', // local
    HOST : 'http://103.108.187.203:8090/', // server

    HUKDIS_LIST: '/api/hukdis',
    HUKDIS_BY_ID_KASUS: '/api/hukdis/',
    HUKDIS_DETAIL: '/api/hukdis/detail/',
    HUKDIS_JENIS: '/api/hukdis/jenisHukdis',

    API_KASUS: '/api/kasus',
    KELOMPOK_KASUS_LIST: '/api/kasus/kelompok',
    STATUS_PROSES_LIST: '/api/kasus/proses',
    STATUS_PERSONAL_LIST: '/api/kasus/statusPersonal',
    VERIRIKASI_KASUS: '/api/kasus/verifikasi',
    KASUS_REPORT: '/api/kasus/report',
    UNIT_KERJA_LIST: '/api/unitKerja', 

    USER_BY_NIP: '/api/user/',
    USER_UPDATE_RIWAYAT_HUKDIS: '/api/user/updateRiwayatHukdis/',

    SUCCESS_RESPONSE_CODE: 200
}