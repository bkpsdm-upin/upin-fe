import axios from 'axios';
import history from '../history';
import {API_LOGIN} from '../config';

const instance = axios.create({
    baseURL: API_LOGIN.HOST,
    headers: {
        'Accept':  'application/json',
        'Content-Type': 'application/json'
    }
});

instance.defaults.withCredentials = false;
instance.interceptors.response.use(response => {    
    return response;
}, error => {
    history.push('/login')
});

export default instance;