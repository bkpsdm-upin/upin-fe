import axios from 'axios';
import history from '../history';
import {END_POINT} from '../config';

const instance = axios.create({
    baseURL: END_POINT.HOST,
    headers: {
        'Accept':  'application/json',
        'Content-Type': 'application/json'
    }
});

instance.defaults.withCredentials = false;
instance.interceptors.response.use(response => {    
    return response;
}, error => {
    
    // history.push('/login')
});

export default instance;