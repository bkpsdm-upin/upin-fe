/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { useLocation, NavLink } from "react-router-dom";

import { Nav } from "react-bootstrap";

import logo from "assets/img/reactlogo.png";

function Sidebar(data) {
  const location = useLocation();
  const user = data.user;
  const image = "";
  const color = "blue";
  const activeRoute = (routeName) => {
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  };

  if(user == undefined) return false;
  return (
    
    <div className="sidebar" data-image={"image"} data-color={color}>
      <div
        className="sidebar-background"
        style={{
          backgroundImage: "url(" + image + ")"
        }}
      />
      <div className="sidebar-wrapper">
        <div className="logo d-flex align-items-center justify-content-start">
          
            <div className="logo-img">
              <img src={require("assets/img/logo.png")} alt="..." width={"24px"} />
            </div>
          
          <a className="simple-text" >
            &nbsp;SiUpin
          </a>
        </div>
        <Nav>
          <li className={activeRoute("/dashboard")} >
            <NavLink
              to={"/dashboard"}
              className="nav-link"
              activeClassName="active"
            >
              <i className="nc-icon nc-chart-pie-35" />
              <p>Dashboard</p>
            </NavLink>
          </li>

          <li className={activeRoute("/ajuankasus")} >
            <NavLink
              to={"/ajuankasus"}
              className="nav-link"
              activeClassName="active"
            >
              <i className="nc-icon nc-notes" />
              <p>Pengajuan Kasus</p>
            </NavLink>
          </li>
          {user.nama == 'admin1' && 
          <li className={activeRoute("/verifikasikasus")} >
            <NavLink
              to={"/verifikasikasus"}
              className="nav-link"
              activeClassName="active"
            >
              <i className="nc-icon nc-puzzle-10" />
              <p>Verifikasi Kasus</p>
            </NavLink>
          </li>  }

          {/* <li className={activeRoute("/panggilan")} >
            <NavLink
              to={"/panggilan"}
              className="nav-link"
              activeClassName="active"
            >
              <i className="nc-icon nc-email-85" />
              <p>Surat Panggilan</p>
            </NavLink>
          </li>         */}

          <li className={activeRoute("/hukdis")} >
            <NavLink
              to={"/hukdis"}
              className="nav-link"
              activeClassName="active"
            >
              <i className="nc-icon nc-atom" />
              <p>Hukuman Disiplin</p>
            </NavLink>
          </li>
            
        </Nav>
      </div>
    </div>
  );
}

export default Sidebar;
