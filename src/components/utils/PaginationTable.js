import React from 'react';
import TablePagination from '@material-ui/core/TablePagination';  

class PaginationTable extends React.Component{   

    handleChangePage = (event, newPage) => {
      this.props.handleChangePage(newPage);
    };

    handleChangeRowsPerPage = (event) => {
      this.props.handleChangeRowsPerPage(event.target.value)
    };

    render() {
        const paging = this.props.paging;
        return (    
            <TablePagination
            component="div"
            count={paging.totalElements}
            page={paging.page}
            onPageChange={this.handleChangePage}
            rowsPerPage={paging.size}
            onRowsPerPageChange={this.handleChangeRowsPerPage}
            />
        );
    }
}

export default PaginationTable;
