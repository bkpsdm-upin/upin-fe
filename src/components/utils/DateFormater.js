import React from "react";
import Moment from 'moment';
const DateFormater = (props) => {
    Moment.locale('id');
    let dt = props.value;
    return(<span>{Moment(dt).format('d MMM YYYY')}</span>)
}

export default DateFormater;