import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "../../../components/Sidebar/Sidebar";
import {
    Table,
    Button,
    Card,
    Form,
    ProgressBar,
    Container,
    Modal,
    Row,
    Col
} from "react-bootstrap";
import Select from 'react-select';
import PaginationData from "../../entity/PaginationData";
import PaginationTable from '../../utils/PaginationTable';
import moment from "moment";
import ExportXls from "components/utils/ExportXls";

class PutusanHukdis extends React.Component {
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            isLoading: false,
            showModal: false,
            showModalDownload: false,
            paginationData: PaginationData,
            kasusModel: {statusVerifikasi: 2},
            dataList: [],
            kelompokKasusList: [],
            statusProsesList: [],
            validated: false,
            tabShow: false,
            selectedId: null,
            tahunKasus: null,
            dataReport: []
        }
    }

    async componentDidMount() {
        await this.props.cekCookies();
        if(!__.isEmpty(this.props.account)) {
            this.fetchKasus();
            this.fetchKelompokKasus();
            this.fetchStatusProses();
        }
    }

    fetchKasus = async() => {   
        this.setState({isFetching:true, dataList: []})    
        const dataSend = {
            page : this.state.paginationData.page,
            size : this.state.paginationData.size,
            kasus: this.state.kasusModel
        }  
        const result = await apiUpin.post(END_POINT.API_KASUS, dataSend);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    dataList: result.data.content,
                    paginationData: {...this.state.paginationData, 
                        page: result.data.page,
                        size: result.data.size,
                        totalPages: result.data.totalPages,
                        totalElements: result.data.totalElements,
                        last: result.data.last
                    },
                    isFetching: false
                });
                   
            }
        }
    }

    fetchKelompokKasus = async() => {   
        this.setState({kelompokKasusList: []})     
        const result = await apiUpin.get(END_POINT.KELOMPOK_KASUS_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data.content;
                let optionKelompok = [{value: "", label: "--Pilih--"}];
                array.map(d => {
                    let obj = {
                        value: d.idKelompokKasus,
                        label: d.ppYangMengatur
                    }
                    optionKelompok.push(obj);
                });
                this.setState({kelompokKasusList: optionKelompok})
            }
        }
    }

    fetchStatusProses = async() => {   
        this.setState({ statusProsesList: []})     
        const result = await apiUpin.get(END_POINT.STATUS_PROSES_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data.content;
                let option = [];
                array.map(d => {
                    let obj = {
                        value: d.idStatusProses,
                        label: d.statusProses
                    }
                    option.push(obj);
                });
                this.setState({statusProsesList: option})
            }
        }
    }

    handleSubmit = async() => {
        this.setState({showModal: false})
        this.fetchKasus();    
        
    }

    setShowModal = () => 
    this.setState({ showModal: true, kasusModel: {statusVerifikasi: 2}})

    detail = (idKasus) => {
        
        history.push({
            pathname: '/hukdis/detail/' + idKasus
        })
    }

    downloadExcel = async() => {
        const {kasusModel} = this.state;
        console.log('param report ====> ', kasusModel)
        this.setState({showModalDownload: true, isLoading: true})
        const result = await apiUpin.post(END_POINT.KASUS_REPORT, kasusModel);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    isLoading: false,
                    dataReport: result.data
                })
            } else {
                this.setState({showModalDownload: false, isLoading: false})
            }
        } else {
            this.setState({showModalDownload: false, isLoading: false})
        }
        
    }

    hideModalDownload = () => {
        this.setState({showModalDownload: false})
    }

    render() {
        const{dataList, kelompokKasusList, statusProsesList, dataReport} = this.state;        
        // const wscols = [
        //     { wch: Math.max(...dataList.map(data => data.asal.length)) },
        //     { wch: Math.max(...dataList.map(data => data.isiSurat.length)) },
        //     { wch: Math.max(...dataList.map(data => data.tanggalSuratAduan.length)) },
        //     { wch: Math.max(...dataList.map(data => data.kelompokKasus.ppYangMengatur.length)) },
        //     {
        //       wch: Math.max(...dataList.map(data => data.statusProses.statusProses.length)) + 3
        //     }
        //   ];        
        //   console.log(Math.max(...dataList.map(customer => customer.asal.length)));
        return(
          <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Putusan Hukuman Disiplin"} />
              <div className="content">
              
                <Container fluid>                
                <Row>
                    <Col md="12">
                        <Card className="strpied-tabled-with-hover">
                        <Card.Header className="text-right">                        
                        
                            <Button variant="success" onClick={() => this.downloadExcel()}>
                                <i className="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel
                            </Button> &nbsp;
                            <Button variant="primary" onClick={() => this.setShowModal()}><i className="fa fa-sort" aria-hidden="true"></i> Filter</Button>
                        </Card.Header>
                        
                        <Card.Body className="table-full-width table-responsive px-0">
                            <Table className="table-hover small">
                            <thead>
                                <tr>
                                <th className="border-0">Asal</th>
                                <th className="border-0">Uraian Kasus</th>                            
                                <th className="border-0">Tanggal Aduan</th>
                                <th className="border-0">Kelompok</th>
                                <th className="border-0">Status</th>
                                <th className="border-0">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                {dataList.map((data, idx) => 
                                    <tr key={idx}>
                                        <td>{data.asal}</td>
                                        <td>{data.isiSurat}</td>
                                        <td>{moment(new Date(data.tanggalSuratAduan)).format('DD-MM-YYYY')}</td>
                                        <td>{data.kelompokKasus == null ? '-' : data.kelompokKasus.ppYangMengatur}</td>
                                        <td>{data.statusProses == null ? '-' : data.statusProses.statusProses}</td>                                    
                                        <td>
                                            <Button variant="info" size="sm" onClick={() => this.detail(data.idKasus)}>Detail</Button>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                            </Table>
                        </Card.Body>
                        </Card>
                        
                        <PaginationTable 
                            paging={this.state.paginationData} 
                            handleChangePage={this.handleChangePage} 
                            handleChangeRowsPerPage={this.handleChangeRowsPerPage} />
                    </Col>
                </Row> 
                {/* Modal Filter */}
                <Modal
                className="modal modal-primary"
                show={this.state.showModal}
                >
                    <Modal.Header >
                    <h5>Filter :</h5>
                    </Modal.Header>               
                    <Modal.Body>
                        <Form>
                            <Form.Group as={Row}>
                                <Col sm="4">
                                    <label>Asal</label>
                                </Col> 
                                <Col sm="8">
                                    <Form.Control type="text" placeholder="Asal Kasus..."
                                    onChange={e => this.handleChangeAsal(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <br />
                            <Form.Group as={Row}>
                                <Col sm="4">
                                    <label>Tahun</label>
                                </Col> 
                                <Col sm="8">
                                    <Form.Control type="number" placeholder="Tahun masuk..." 
                                    onChange={e => this.handleChangeTahun(e.target.value)} />  
                                    {this.state.validated && <div className="invalid-feedback d-block">
                                        Wrong value for year...
                                    </div>}
                                </Col>
                            </Form.Group>
                            <br />
                            <Form.Group as={Row}>
                                <Col sm="4">
                                    <label>Kelompok Kasus</label>
                                </Col> 
                                <Col sm="8">
                                    <Select 
                                        options={kelompokKasusList}
                                        defaultValue=""                                    
                                        onChange={e => this.handleChangeKelompok(e)} />
                                </Col>
                            </Form.Group>
                            <br />
                            <Form.Group as={Row}>
                                <Col sm="4">
                                    <label>Status Proses</label>
                                </Col> 
                                <Col sm="8">
                                    <Select 
                                        options={statusProsesList}
                                        defaultValue=""                                    
                                        onChange={e => this.handleChangeStatus(e)} />
                                </Col>
                            </Form.Group>
                            <br />
                        </Form>
                    </Modal.Body>
                    <div className="modal-footer">
                        <Button
                        variant="danger"
                        onClick={() => this.setState({showModal:false}) }
                        >
                        Batal
                        </Button>
                        <Button type="submit"
                        variant="primary" onClick={this.handleSubmit}
                        >
                        OK
                        </Button>
                    </div>
                </Modal>
                {/* ENd Modal Filter */}

                {/* Modal Loading*/}
                <Modal
                    className="modal modal-primary"
                    backdrop="static"
                    keyboard={false}
                    show={this.state.showModalDownload}
                    >               
                    <Modal.Body className="text-center">
                        {this.state.isLoading ? <>
                        <h4>Mohon Tunggu...</h4>
                        <ProgressBar variant="secondary" animated now={100} /></> :
                        <ExportXls
                            onClick={() => this.setState({showModalDownload: false})}
                            csvData={dataReport}
                            fileName={"Pemerintah Kab Klaten Rekapitulasi Hukdis Tahun " + this.state.tahunKasus}
                            // wscols={wscols}
                        />
                        }
                    </Modal.Body>
                </Modal>
                {/* End Modal */}
               
                </Container>                 
              </div>
              <Footer />
            </div>
            <LoaderModal isFetching={this.state.isFetching} />
          </div>
        )
    }

    handleChangePage = (newPage) => {
        this.state.paginationData.page = newPage;
        this.fetchKasus();
    }
    handleChangeRowsPerPage = (size) => {
        this.state.paginationData.size = size;
        this.fetchKasus();
    }
    handleChangeAsal = (value) => {
        this.setState(prevState => ({ 
            kasusModel: {...prevState.kasusModel, asal: value} }
        ))
    }
    handleChangeTahun = (value) => {
        if (value.length != 4) {
            this.setState({validated: true})
            return false;
          } 

          
        this.setState(prevState => ({ 
            validated: false,
            tahunKasus: value,
            kasusModel: {...prevState.kasusModel, tanggalSuratAduan: value + '-01' + '-01'} }
        ))
    }
    handleChangeKelompok = (e) => {       
        this.setState(prevState => ({ 
            kasusModel: {...prevState.kasusModel, kelompokKasus: {idKelompokKasus: e.value, ppYangMengatur: e.label}} }
        ))
    }
    handleChangeStatus = (e) => {
        this.setState(prevState => ({ 
            kasusModel: {...prevState.kasusModel, statusProses: {idStatusProses: e.value, statusProses: e.label}} }
        ))
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(PutusanHukdis);