import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __, { times } from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "../../../components/Sidebar/Sidebar";
import {
    Table,
    Button,
    Form,
    InputGroup,
    Container,
    Modal,
    Row,
    Col,
    Tabs, Tab
} from "react-bootstrap";
import moment from "moment";
import KasusModel from "components/entity/KasusModel";
import NotificationAlert from "react-notification-alert";

class DetailPutusan extends React.Component {
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            idKasus: null,
            jenisHukdis: [],
            data: {},
            dataPenggugat: [],
            dataTergugat: [],
            tabKey: 'putusan',
            kasus: KasusModel,
            arrayPutusan: [],
            editable: false,
            showModal: false
        }
    }

    async componentDidMount() {
        await this.props.cekCookies();
        if(!__.isEmpty(this.props.account)) {
            this.fetchJenisHukdis();
            const id = this.props.match.params.id;
            if(!__.isUndefined(id)) {
                this.setState({ idKasus: id}) 
                this.fetchKasus(id);
                this.fetchHukdis(id);                
            }
        }
    }

    fetchJenisHukdis = async() => {
        this.setState({ jenisHukdis: []})
        const result = await apiUpin.get(END_POINT.HUKDIS_JENIS);
        if(result) {           
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
               this.setState({ jenisHukdis: result.data.content})          
            }
        }        
    }

    fetchKasus = async(id) => {
        this.setState({ kasus: KasusModel})
        const result = await apiUpin.get(END_POINT.API_KASUS + '/'+ id);
        if(result) {           
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
               this.setState({ kasus: result.data})          
            }
        }        
    }

    fetchHukdis = async(id) => {
        this.setState({ isFetching: true, data: {}})
        const result = await apiUpin.get(END_POINT.HUKDIS_DETAIL + id);
        if(result) {            
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const data = JSON.parse(result.data.data)
                if(!__.isEmpty(result.data.data)) {
                    this.setState({ 
                        dataPenggugat: data.penggugat,
                        dataTergugat: data.tergugat,
                        arrayPutusan: [...data.tergugat, ...data.penggugat]
                    })
                }                
            }
            this.setState({ isFetching: false})
        }
        
    }

    handleChangeJenisHukdis = (value) => {

    }

    handleChangeNoSK = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, noSkPutusan: value}})
    }
    handleChangeTglSK = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, tglSkPutusan: value}})
    }
    handleChangePejabat = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, pejabatYgBerwenang: value}})
    }
    handleChangeJenisHukdis = (kode) => {
        const value = this.state.jenisHukdis.find(v => v.kodeHukdis === kode);
        this.setState({data: {...this.state.data, putusanHukdis: value.kodeHukdis, lamaHukdisTh: value.lamaHukdis, lamaHukdisBl: 0}})
    }
    handleLamaHukdisTh = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, lamaHukdisTh: value}})
    }
    handleLamaHukdisBl = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, lamaHukdisBl: value}})
    }
    handleChangeBerlakuDari = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, berlakuDari: value}})
    }
    handleChangeBerlakuSampai = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, berlakuSampai: value}})
    }
    handleChangeTppStart = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, tppBerlakuDari: value}})
    }
    handleChangeTppEnd = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, tppBerlakuSampai: value}})
    }
    handleChangeKetHukdis = (value) => {
        if(value != null || value != "")
        this.setState({data: {...this.state.data, keterangan: value}})
    }    

    renderElePutusan() {
        const {jenisHukdis, data} = this.state;
        if(!__.isEmpty(data)) {
            return(
                <Table responsive="md">
                    <tbody>
                        <tr>
                            <td width={'15%'}>NIP</td><td width={'10px'}>:</td><td>{data.nip}</td>
                        </tr>
                        <tr>
                            <td>Nama</td><td>:</td><td>{this.renderNama(data)}</td>
                        </tr>
                        <tr>
                            <td>Pangkat/Golongan</td><td>:</td>{data.golru === undefined ? <td></td>:<td>{data.golru.pangkat + ' / ' + data.golru.golongan}</td>}
                        </tr>
                        <tr>                                         
                            <td>Jabatan</td><td>:</td>{data.jabatan === undefined ? <td></td>:<td>{data.jabatan.jabatan}</td>}
                        </tr>
                        <tr>
                            <td>Unit Kerja</td><td>:</td><td>{data.unitKerja == undefined ? "" : data.unitKerja.nama}</td>
                        </tr>
                        <tr style={{backgroundColor: '#dddddd'}}>
                            <td colSpan={3}><strong>SK PUTUSAN</strong></td>
                        </tr>
                        <tr>
                            <td>Nomor SK</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="6">
                                        <Form.Group>
                                            <Form.Control
                                            value={data.noSkPutusan || ""}
                                            onChange={e => this.handleChangeNoSK(e.target.value)}
                                            type="text" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col md="2" className="justify-content-center align-self-center"><span>Tanggal SK :</span></Col>
                                    <Col md="4">
                                        <Form.Group>
                                            <Form.Control
                                            value={moment(new Date(data.tglSkPutusan)).format('YYYY-MM-DD') || null}
                                            onChange={e => this.handleChangeTglSK(e.target.value)}
                                            type="date" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                 
                            </td>
                        </tr>
                        <tr>
                            <td>Pejabat Yang Berwenang</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="6">
                                    <Form.Group>
                                        <Form.Control
                                        value={data.pejabatYgBerwenang || ""}
                                        onChange={e => this.handleChangePejabat(e.target.value)}
                                        type="text" disabled={!this.state.editable} 
                                        ></Form.Control>
                                    </Form.Group>
                                    </Col>
                                </Row>
                                
                            </td>
                        </tr>
                        {this.state.kasus.kelompokKasus.idKelompokKasus == 'A' && <>
                        <tr>
                            <td>Jenis Hukdis</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="6">
                                    <Form.Group>
                                        <Form.Control
                                        defaultValue={data.putusanHukdis}
                                        onChange={e => this.handleChangeJenisHukdis(e.target.value)}
                                        as="select" disabled={!this.state.editable} 
                                        >
                                            {jenisHukdis.map((val,idx) => 
                                                <option key={idx} value={val.kodeHukdis}>{val.hukdis}</option>
                                            )}                                            
                                        </Form.Control>
                                    </Form.Group>
                                    </Col>
                                </Row>
                            </td>
                        </tr>
                        <tr>
                            <td>Lama Hukdis</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="4">
                                        <InputGroup className="mb-3">
                                        <Form.Control
                                        type="number"
                                        defaultValue={data.lamaHukdisTh}
                                        onChange={e => this.handleLamaHukdisTh(e.target.value)}
                                        aria-describedby="tahun" disabled={!this.state.editable} 
                                        />
                                        <InputGroup.Text id="tahun">tahun</InputGroup.Text>
                                        </InputGroup>
                                    </Col>
                                    <Col md="4">
                                        <InputGroup className="mb-3">
                                        <Form.Control
                                        type="number"
                                        onChange={e => this.handleLamaHukdisBl(e.target.value)}
                                        defaultValue={data.lamaHukdisBl}
                                        aria-describedby="bulan" disabled={!this.state.editable} 
                                        />
                                        <InputGroup.Text id="bulan">bulan</InputGroup.Text>
                                        </InputGroup>
                                    </Col>
                                </Row>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>Berlaku dari tanggal</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="4">
                                        <Form.Group>
                                            <Form.Control
                                            defaultValue={moment(new Date(data.berlakuDari)).format('YYYY-MM-DD')}
                                            onChange={e => this.handleChangeBerlakuDari(e.target.value)}
                                            type="date" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col md="2" className="justify-content-center align-self-center text-center">s/d</Col>
                                    <Col md="4">
                                        <Form.Group>
                                            <Form.Control
                                            defaultValue={moment(new Date(data.berlakuSampai)).format('YYYY-MM-DD')}
                                            onChange={e => this.handleChangeBerlakuSampai(e.target.value)}
                                            type="date" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>Penundaan TPP</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="4">
                                        <Form.Group>
                                            <Form.Control
                                            defaultValue={moment(new Date(data.tppBerlakuDari)).format('YYYY-MM-DD')}
                                            onChange={e => this.handleChangeTppStart(e.target.value)}
                                            type="date" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col md="2" className="justify-content-center align-self-center text-center">s/d</Col>
                                    <Col md="4">
                                        <Form.Group>
                                            <Form.Control
                                            defaultValue={moment(new Date(data.tppBerlakuSampai)).format('YYYY-MM-DD')}
                                            onChange={e => this.handleChangeTppEnd(e.target.value)}
                                            type="date" disabled={!this.state.editable} 
                                            ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                
                            </td>
                        </tr>
                        </>}
                        <tr>
                            <td>Keterangan</td><td>:</td>
                            <td>
                                <Row>
                                    <Col md="10">
                                    <Form.Group>
                                        <Form.Control
                                        defaultValue={data.keterangan}
                                        onChange={e => this.handleChangeKetHukdis(e.target.value)}
                                        as="textarea" rows={2} disabled={!this.state.editable} 
                                        ></Form.Control>
                                    </Form.Group>
                                    </Col>
                                </Row>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>File SK</td><td>:</td>
                            <td>
                            <input type="file" id="myFile" name="filename2" />
                                
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td><td></td><td>
                            <Row>
                                <Col md="6" className="text-right">
                                    {!this.state.editable ?
                                    <Button variant="warning" onClick={() => this.setState({editable: true})}>Edit</Button> :
                                    <Button variant="secondary" onClick={() => this.setState({editable: false})}>Batal</Button>
                                    }
                                </Col>
                                <Col md="6" className="text-right">
                                    {this.state.editable && 
                                    <Button variant="primary" 
                                    onClick={() => this.setState({showModal: true})}>
                                        Simpan
                                    </Button>}
                                    
                                </Col>
                            </Row>
                            </td>
                        </tr>
                    
                    </tfoot>
                </Table>
            )
        }
        
    }

    handleChangePerson = (value) => {
        if(value == '' || null) {
            this.setState({data: {}})
        } else {
            this.setState({data: this.state.arrayPutusan.find(data => data.idHukdis == value)})
        }
    }

    submitClick = async() => {
        this.setState({isFetching: true, showModal: false})
        const result = await apiUpin.put(END_POINT.HUKDIS_LIST, JSON.stringify(this.state.data));
        if(result) {           
            if(result.data.success) {
                // this.setState({editable: false}) 
                // this.notify("primary", "Data Berhasil Disimpan...");
                this.updateKasus();
            } else {
                this.notify("danger", result.data.message) 
                this.setState({isFetching:false})                
            }        
                   
        }

    }

    updateRiwayatHukdis = async(nip, kode) => {
        const result = await apiUpin.put(END_POINT.USER_UPDATE_RIWAYAT_HUKDIS + nip +'/'+ kode );
        if(result) {
            if(result.data.success) {
                this.setState({editable: false }) 
                this.notify("primary", "Data Berhasil Disimpan...");
            } else {
                this.notify("danger", result.data.message);                             
            }        
            this.setState({isFetching:false})     
        } 
    }

    updateKasus = async() => {
        let {kasus, data} = this.state;
        const result = await apiUpin.put(END_POINT.API_KASUS, kasus = {
            ...kasus, statusProses: {idStatusProses: '03'}
        });
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                if(kasus.kelompokKasus.idKelompokKasus == 'A') {
                    this.updateRiwayatHukdis(data.nip, 2);
                } else {
                    this.setState({editable: false, isFetching:false }) 
                    this.notify("primary", "Data Berhasil Disimpan...");
                }
               
            } else {
                this.notify('danger', result.message)
                this.setState({isFetching:false}) 
            }               
        }
    }

    handleBack = () => {
        if(window.confirm('Yakin keluar dari halaman ini ?')) {
            history.push({
                pathname: '/hukdis'
            })
        }
        
    }

    render() {
        const {kasus, dataPenggugat, dataTergugat, arrayPutusan} = this.state;
        const btnStyle = {
            position: 'absolute',
            top: '2px',
            right: '10px',
            border: 'none',
        }
        return(
            <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Putusan Hukuman Disiplin"} />
              <div className="content">
                <Container fluid style={{position: 'relative'}}> 
                <button style={btnStyle} className="btn btn-secondary" onClick={this.handleBack} alt="close"><i className="fa fa-arrow-left"></i> Kembali</button>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.tabKey}
                    onSelect={(k) => this.setState({ tabKey: k})}
                    className="mb-3"
                    >
                    <Tab eventKey="data" title="Data Umum">
                        <Table responsive="md">
                        <tbody>
                            <tr>
                                <td width={'15%'}>Tanggal Masuk</td><td width={'10px'}>:</td><td>{moment(new Date(kasus.tanggalSuratAduan)).format('DD/MM/YYYY')}</td>
                            </tr>
                            <tr>
                                <td>Nomor Surat</td><td>:</td><td>{kasus.noSuratAduan}</td>
                            </tr>
                            <tr>
                                <td>Asal</td><td>:</td><td>{kasus.asal}</td>
                            </tr>
                            <tr>
                                <td>Isi Surat</td><td>:</td><td>{kasus.isiSurat}</td>
                            </tr>
                            <tr>
                                <td>Jenis</td><td>:</td><td>{kasus.kelompokKasus.ppYangMengatur}</td>
                            </tr>
                            <tr>
                                <td>Penggugat</td><td>:</td>
                                <td>
                                    {!__.isEmpty(dataPenggugat) ? <Table >
                                        <thead><tr><th width={'12px'} style={{color: '#000000'}}>No</th>
                                        <th style={{color: '#000000'}}>Nama</th></tr></thead>
                                        <tbody>
                                            {dataPenggugat.map((value, key) => 
                                            <tr key={key}>
                                                <td>{(key+1)}.</td>
                                                <td>{this.renderNama(value)}</td>
                                            </tr>
                                            )}
                                        </tbody>
                                    </Table> : ""}
                                </td>
                            </tr>
                            <tr>
                                <td>Tergugat</td><td>:</td>
                                <td>
                                    {!__.isEmpty(dataTergugat) ? <Table responsive="md">
                                        <thead><tr><th width={'12px'} style={{color: '#000000'}}>No</th><th style={{color: '#000000'}}>Nama</th></tr></thead>
                                        <tbody>
                                            {dataTergugat.map((value, key) => 
                                            <tr key={key}>
                                                <td>{(key+1)}.</td>
                                                <td>{this.renderNama(value)}</td>
                                            </tr>
                                            )}
                                        </tbody>
                                    </Table> : ""}
                                </td>
                            </tr>
                        </tbody>
                        </Table>
                    </Tab>
                    <Tab eventKey="putusan" title="Putusan Hukdis">
                        <Form>
                            <Row>                            
                                <Col md="4">
                                    <Form.Group>
                                        <Form.Control
                                        defaultValue=""
                                        onChange={e => this.handleChangePerson(e.target.value)}
                                        as="select"
                                        >
                                            <option value="">Pilih Opsi...</option>
                                            {arrayPutusan.map((value, key) =>
                                            <option key={key} value={value.idHukdis}>{value.nama} </option>
                                            )}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>                                
                            </Row>
                            {this.renderElePutusan()}
                        </Form>
                    </Tab>
                    <Tab eventKey="riwayat" title="Riwayat Panggilan">
                        <p>Belum ada riwayat...</p>
                    </Tab>
                </Tabs>
                </Container>
                </div>
                <Footer />
              </div>
              <LoaderModal isFetching={this.state.isFetching} />
                {/* Mini Modal */}
                <Modal
                    className="modal modal-primary"
                    show={this.state.showModal}
                    onHide={() => this.setState({showModal:false}) }
                    >
                    <Modal.Header className="justify-content-center">
                        <div className="modal-profile">
                        <i className="nc-icon nc-bulb-63"></i>
                        </div>
                    </Modal.Header>
                    <Modal.Body className="text-center">
                        <p>Kasus akan otomatis terupdate menjadi <strong>SELESAI</strong>.</p>
                        <p>Apakah anda yakin ?</p>
                    </Modal.Body>
                    <div className="modal-footer">
                        <Button
                        variant="danger"
                        onClick={() => this.setState({showModal:false}) }
                        >
                        Tidak
                        </Button>
                        <Button
                        variant="primary"
                        onClick={this.submitClick}
                        >
                        Ya
                        </Button>
                    </div>
                </Modal>
                {/* End Modal */} 
              <NotificationAlert ref={this.notifRef} zIndex={999} /> 
            </div>
        )
    }

    renderNama =(data) => {
        let nama = (data.glrDepan == null || undefined ? "" : data.glrDepan+". ") + data.nama + (data.glrBelakang == null || undefined ? "" : ", "+data.glrBelakang);      
        return nama;
    }
    notify = (type, message) => {
        var options = {};
        options = {
            place: 'tc',
            message: (
                <div>{message}</div>
            ),
            type: type,
            icon: "nc-icon nc-bell-55",
            autoDismiss: 15
        }
        this.notifRef.current.notificationAlert(options);
    }

}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(DetailPutusan);

