import React from 'react';
import { connect } from 'react-redux';
import {cekCookies} from '../../action';
import __ from 'lodash';
import history from '../../history';
import LoaderModal from '../Loader/LoaderModal';
import { END_POINT } from '../../config';
import apiUpin from '../../apis/apiUpin';


class DataHukdis extends React.Component{
    constructor(props) {
        super(props);
        this.state = { 
            isFetching: false,
            datalist: [],
            page: 0,
            size: 20
        }
    }

    async componentDidMount() { 
        await this.props.cekCookies();    
        if(!__.isEmpty(this.props.account)) {
            this.fetchHukdis();
            
        }
    }

    fetchHukdis = async() => {   
        this.setState({isFetching:true, datalist: []})     
        const result = await apiUpin.get(END_POINT.HUKDIS_LIST, {
            params: { page: this.state.page, 
                    size: this.state.size, }
        });
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                console.log(result.data)
                this.setState({
                    dataList: result.data.content,
                    paginationData: {...this.state.paginationData, 
                        page: result.data.page,
                        size: result.data.size,
                        totalPages: result.data.totalPages,
                        totalElements: result.data.totalElements,
                        last: result.data.last
                    },
                    isFetching: false
                });
                   
            }
        }
    }

    handleChangePage = async(newPage) => {
        this.state.page = newPage;
        this.fetchHukdis();
    }

    handleChangeRowsPerPage = (size) => {
        this.state.page = 0;
        this.state.size = size;
        this.fetchHukdis();
    }

    render() {
        return(
          <>
            <LoaderModal isFetching={this.state.isFetching} /> <PaginationTable paging={this.state.paginationData} handleChangePage={this.handleChangePage} handleChangeRowsPerPage={this.handleChangeRowsPerPage} />
          </>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(DataHukdis);