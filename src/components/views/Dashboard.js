import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../action';
import __ from 'lodash';
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        isFetching: false,
        user: {}
    }
  }
  async componentDidMount() {
    await this.props.cekCookies();
    if(!__.isEmpty(this.props.account)) {
      this.setState({user: this.props.account})
    }
  }
  render() {
    const {user} = this.state;
    return (
      <div className="wrapper">
          <Sidebar user={user} />
          <div className="main-panel" >
            <AdminNavbar title={"Dashboard"} />
            <div className="content">
            <Container fluid>
              <h4>Selamat Datang, {user.nama}</h4>
              <Row>
                <Col lg="3" sm="6">
                  <Card className="card-stats">
                    <Card.Body>
                      <Row>
                        <Col xs="5">
                          <div className="icon-big text-center icon-warning">
                            <i className="nc-icon nc-chart text-warning"></i>
                          </div>
                        </Col>
                        <Col xs="7">
                          <div className="numbers">
                            <p className="card-category">Total</p>
                            <Card.Title as="h4">15</Card.Title>
                          </div>
                        </Col>
                      </Row>
                    </Card.Body>
                    <Card.Footer>
                      <hr></hr>
                      <div className="stats">
                        <i className="fas fa-redo mr-1"></i>
                        Perlu Verifikasi
                      </div>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col lg="3" sm="6">
                  <Card className="card-stats">
                    <Card.Body>
                      <Row>
                        <Col xs="5">
                          <div className="icon-big text-center icon-warning">
                            <i className="nc-icon nc-light-3 text-success"></i>
                          </div>
                        </Col>
                        <Col xs="7">
                          <div className="numbers">
                            <p className="card-category">Total</p>
                            <Card.Title as="h4">1</Card.Title>
                          </div>
                        </Col>
                      </Row>
                    </Card.Body>
                    <Card.Footer>
                      <hr></hr>
                      <div className="stats">
                        <i className="fa fa-calendar-alt mr-1"></i>
                        Belum Diproses
                      </div>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col lg="3" sm="6">
                  <Card className="card-stats">
                    <Card.Body>
                      <Row>
                        <Col xs="5">
                          <div className="icon-big text-center icon-warning">
                            <i className="nc-icon nc-vector text-danger"></i>
                          </div>
                        </Col>
                        <Col xs="7">
                          <div className="numbers">
                            <p className="card-category">Total</p>
                            <Card.Title as="h4">5</Card.Title>
                          </div>
                        </Col>
                      </Row>
                    </Card.Body>
                    <Card.Footer>
                      <hr></hr>
                      <div className="stats">
                        <i className="fa fa-redo mr-1"></i>
                        Sedang Berlangsung
                      </div>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col lg="3" sm="6">
                  <Card className="card-stats">
                    <Card.Body>
                      <Row>
                        <Col xs="5">
                          <div className="icon-big text-center icon-warning">
                            <i className="nc-icon nc-favourite-28 text-primary"></i>
                          </div>
                        </Col>
                        <Col xs="7">
                          <div className="numbers">
                            <p className="card-category">Total</p>
                            <Card.Title as="h4">17</Card.Title>
                          </div>
                        </Col>
                      </Row>
                    </Card.Body>
                    <Card.Footer>
                      <hr></hr>
                      <div className="stats">
                        <i className="fa fa-redo mr-1"></i>
                        Selesai
                      </div>
                    </Card.Footer>
                  </Card>
                </Col>
              </Row>
              
              
            </Container>
              
            </div>
            
            <Footer />
          </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return{
      account: state.account        
  };
}
export default connect(mapStateToProps, {cekCookies})(Dashboard);

