import React from 'react';
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import Select from 'react-select';
import KasusModel from '../../entity/KasusModel';
import {
    Button,
    Card,
    Form,
    Container,
    Row,
    Col,
    Modal
} from "react-bootstrap";
import NotificationAlert from "react-notification-alert";
import moment from 'moment';

class SuratPanggilan extends React.Component{
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
        }
    }

    async componentDidMount() { 
        await this.props.cekCookies();    
        if(!__.isEmpty(this.props.account)) {
            
        }        
    }

    render(){
        return(
          <div className="wrapper">
            <Sidebar />
            <div className="main-panel">
              <AdminNavbar title={"Buat Surat Panggilan"} />
              <div className="content">
                <Container fluid>

                </Container>
              </div>
              <Footer />
            </div>
            
            <LoaderModal isFetching={this.state.isFetching} />
          </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(SuratPanggilan);