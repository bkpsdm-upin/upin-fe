import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "../../../components/Sidebar/Sidebar";
import {
    Table,
    Button,
    Card,
    Modal,
    Container,
    Row,
    Col
} from "react-bootstrap";
import KasusModel from "components/entity/KasusModel";
import NotificationAlert from "react-notification-alert";

class VerifikasiKasusDetail extends React.Component {
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            showModal: false,
            dataKasus: KasusModel,
            dataHukdis: [],
            action: ''
        }
    }

    async componentDidMount() {
        await this.props.cekCookies();
        if(!__.isEmpty(this.props.account)) {
           const params = this.props.location.params;
           if(__.isUndefined(params)) {
            history.push({
                pathname: '/verifikasikasus'
            })
           } else {            
            this.setState({ dataKasus: params });
            this.fetchKasusDetail(params.idKasus);
           }
        }
    }

    fetchKasusDetail = async(idKasus) => {
        this.setState({isFetching:true, dataList: []})     
        const result = await apiUpin.get(END_POINT.HUKDIS_BY_ID_KASUS + idKasus);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    dataHukdis: result.data,                    
                    isFetching: false
                });                   
            }
        }
    }

    
    render() {
        const {dataHukdis, dataKasus} = this.state;
        return(
            <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Detail Kasus"} />
              <div className="content">
                <Container fluid>
                <Row>
                <Col md="12">
                    <Card className="strpied-tabled-with-hover">
                    <Card.Header>                        
                        <p>
                        &#10070; {dataKasus.isiSurat}
                        </p>
                        <p>
                        &#10070; Asal: {dataKasus.asal}
                        </p>
                        
                        <p>
                        &#10070; Kelompok: {dataKasus.kelompokKasus.ppYangMengatur + ' / ' + dataKasus.statusProses.statusProses}
                        </p>
                    </Card.Header>
                    <Card.Body className="table-full-width table-responsive px-0"></Card.Body>
                    </Card>

                    {dataHukdis.map((data, key) =>                     
                        <Card key={key}>
                            <Card.Header> 
                                <Card.Title as="h5">{this.renderNama(data, key)}</Card.Title>
                            </Card.Header>
                            <Card.Body className="table-full-width table-responsive px-0">
                                <Table className="table-hover">
                                <tbody>
                                    <tr>
                                        <td width={'20%'}>NIP</td><td width={'10px'}>:</td><td>{data.nip}</td>
                                    </tr>
                                    <tr>                                        
                                        <td>Pangkat/Golongan</td><td>:</td>{data.golru === null ? <td></td>:<td>{data.golru.pangkat + ' / ' + data.golru.golongan}</td>}
                                    </tr>
                                    <tr>                                         
                                        <td>Jabatan</td><td>:</td>{data.jabatan === null ? <td></td>:<td>{data.jabatan.jabatan}</td>}
                                    </tr>
                                    <tr>
                                        <td>Unit Kerja</td><td>:</td><td>{data.unitKerja == null ? "" : data.unitKerja.nama}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td><td>:</td><td>{data.alamat}</td>
                                    </tr>
                                    <tr>
                                        <td>Status </td><td>:</td><td>{data.statusPersonal.status}</td>
                                    </tr>
                                </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                    )}
                    
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md={6}>
                                    <Button variant="danger" onClick={this.handleBack}><i className="fa fa-arrow-left"></i> kembali</Button>
                                </Col>
                                <Col md={6}>
                                    <Row >
                                        <Col md={8} className="text-right">
                                            <Button variant="info" value="1" onClick={(e) => this.confirm(e)}>Terima</Button>
                                        </Col>
                                        <Col md={4} className="text-right">
                                            <Button variant="default" value="2" onClick={(e) => this.confirm(e)}>Tolak</Button>
                                        </Col>
                                    </Row>
                                    
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                   
                </Col>
                </Row>                
                </Container>                
              </div>
              <Footer />
            </div>
            <LoaderModal isFetching={this.state.isFetching} />
            {/* Mini Modal */}
            <Modal
                className="modal-mini modal-primary"
                show={this.state.showModal}
                onHide={() => this.setState({showModal:false}) }
                >
                <Modal.Header className="justify-content-center">
                    <div className="modal-profile">
                    <i className="nc-icon nc-bulb-63"></i>
                    </div>
                </Modal.Header>
                <Modal.Body className="text-center">
                    <p>Apakah anda yakin {this.state.action} ? </p>
                    
                    {this.state.action == 'Tolak Kasus' && <div className="form-group">
                        <label >Alasan:</label>
                        <textarea className="form-control" rows="3" id="comment"></textarea>
                    </div>
                    }
                </Modal.Body>
                <div className="modal-footer">
                    <Button
                    variant="danger"
                    onClick={() => this.setState({showModal:false}) }
                    >
                    Tidak
                    </Button>
                    <Button
                    variant="primary"
                    onClick={this.handleSubmit}
                    >
                    Ya
                    </Button>
                </div>
            </Modal>
            {/* End Modal */}   
            <NotificationAlert ref={this.notifRef} zIndex={999} />
            </div>
        )
    }

    renderNama =(data, key) => {
        let no = (key + 1);
        let nama = (data.glrDepan === null ? "" : data.glrDepan+" ") + data.nama + (data.glrBelakang === null ? "" : ", "+data.glrBelakang);
        
        return no + '. ' +nama;
    }

    confirm = (e) => {
        const {dataKasus} = this.state;
        const value = e.target.value;
        this.setState({
            showModal: true,
            action: value === '1' ? 'Terima Kasus' : 'Tolak Kasus',
            dataKasus: { ...dataKasus,
                idKasus: dataKasus.idKasus,
                statusVerifikasi: value,
                verifiedAt: new Date(),
                verifiedBy: this.props.account.nip
            }
        })
    }

    handleSubmit = async() => {
        const {dataKasus} = this.state;
        this.setState({isFetching:true, showModal: false})
        const result = await apiUpin.post(END_POINT.VERIRIKASI_KASUS, dataKasus);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                if(result.data.success) {
                    history.push({
                        pathname: '/verifikasikasus',
                        state: dataKasus.idKasus    
                    })  
                } else {
                    this.notify("danger", result.message)                    
                }   
                this.setState({isFetching:false})        
            }     
        }
    }

    handleBack = () => {
        const {dataKasus} = this.state;
        history.push({
            pathname: '/verifikasikasus',
            state: {idKasus: dataKasus.idKasus} 
        })
    }

    notify = (type, message) => {
        var options = {};
        options = {
            place: 'tc',
            message: (
                <div>{message}</div>
            ),
            type: type,
            icon: "nc-icon nc-bell-55",
            autoDismiss: 7
        }
        this.notifRef.current.notificationAlert(options);
    }

}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(VerifikasiKasusDetail);