import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "../../../components/Sidebar/Sidebar";
import {
    Table,
    Button,
    Card,
    Form,
    Container,
    Row,
    Col
} from "react-bootstrap";
import PaginationData from "../../entity/PaginationData";
import PaginationTable from '../../utils/PaginationTable';
import moment from "moment";
import NotificationAlert from "react-notification-alert";

class VerifikasiKasus extends React.Component {
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            showModal: false,
            paginationData: PaginationData,
            dataList: [],
            dataHukdis: []
        }
    }

    async componentDidMount() {
        await this.props.cekCookies();
        if(!__.isEmpty(this.props.account)) {
            this.fetchKasus();
        }
    }

    fetchKasus = async() => {   
        this.setState({isFetching:true, dataList: []})    
        const dataSend = {
            page : this.state.paginationData.page,
            size : this.state.paginationData.size,
            kasus: {
                statusVerifikasi: null,
              }
        } 
        
        const result = await apiUpin.post(END_POINT.API_KASUS, dataSend);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    dataList: result.data.content,
                    paginationData: {...this.state.paginationData, 
                        page: result.data.page,
                        size: result.data.size,
                        totalPages: result.data.totalPages,
                        totalElements: result.data.totalElements,
                        last: result.data.last
                    },
                    isFetching: false
                });
                   
            }
        }
    }

    handleChangePage = (newPage) => {
        this.state.paginationData.page = newPage;
        this.fetchKasus();
    }

    handleChangeRowsPerPage = (size) => {
        this.state.paginationData.size = size;
        this.fetchKasus();
    }

    verifikasi = (data) => {
        history.push({
            pathname: '/verifikasikasus/detail',
            params: data
        })
    }

    
    render() {
        const {dataList} = this.state;
        return(
            <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Verifikasi Kasus"} />
              <div className="content">
                <Container fluid>
                <Row>
                <Col md="12">
                    <Card className="strpied-tabled-with-hover">
                    <Card.Body className="table-full-width table-responsive px-0">
                        <Table className="table-hover small">
                        <thead>
                            <tr>
                            <th className="border-0">Asal</th>
                            <th className="border-0">Uraian Kasus</th>                            
                            <th className="border-0">Tanggal Aduan</th>
                            <th className="border-0">Kelompok</th>
                            <th className="border-0">Status</th>
                            <th className="border-0">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            {dataList.map((data, idx) => 
                                <tr key={idx}>
                                    <td>{data.asal}</td>
                                    <td>{data.isiSurat}</td>
                                    <td>{moment(new Date(data.tanggalSuratAduan)).format('DD-MM-YYYY')}</td>
                                    <td>{data.kelompokKasus == null ? '-' : data.kelompokKasus.ppYangMengatur}</td>
                                    <td>{data.statusProses == null ? '-' : data.statusProses.statusProses}</td>                                    
                                    <td>
                                        <Button variant="default" size="sm" onClick={() => this.verifikasi(data)}>Verifikasi</Button>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                        </Table>
                    </Card.Body>
                    </Card>
                    
                    <PaginationTable 
                        paging={this.state.paginationData} 
                        handleChangePage={this.handleChangePage} 
                        handleChangeRowsPerPage={this.handleChangeRowsPerPage} />
                </Col>
                </Row>                
                </Container>                
              </div>
              <Footer />
            </div>
            <LoaderModal isFetching={this.state.isFetching} />
            <NotificationAlert ref={this.notifRef} zIndex={999} /> 
            </div>
        )
    }

    notify = (type, message) => {
        var options = {};
        options = {
            place: 'tc',
            message: (
                <div>{message}</div>
            ),
            type: type,
            icon: "nc-icon nc-bell-55",
            autoDismiss: 15
        }
        this.notifRef.current.notificationAlert(options);
    }

}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(VerifikasiKasus);