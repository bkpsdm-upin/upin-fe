import React from 'react';
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import Select from 'react-select';
import KasusModel from '../../entity/KasusModel';
import {
    Button,
    Card,
    Form,
    Container,
    Row,
    Col,
    Modal
} from "react-bootstrap";
import NotificationAlert from "react-notification-alert";
import moment from 'moment';

class AjuanKasus extends React.Component{
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            kasusModel: KasusModel,
            unitKerja: [],
            kelompokKasusList: [],
            statusProsesList: [],
            unitKerjaList: [],
            validateNomor: true,
            validateTanggal: false,
            validateAsal: true,
            validateKelompok: true,
            validateStatus: true,
            setShowModal: false
        }
    }

    async componentDidMount() { 
        await this.props.cekCookies();    
        if(!__.isEmpty(this.props.account)) {
            this.fetchKelompokKasus();
            this.fetchStatusProses();
            this.fetchUnitKerja();
            const location = this.props.location;
            if(location.state !== null) {
                this.fetchKasusById(location.state.idKasus);
            }
        }        
    }

    notify = (message) => {
        var options = {};
        options = {
            place: 'tc',
            message: (
                <div>{message}</div>
            ),
            type: "danger",
            icon: "nc-icon nc-bell-55",
            autoDismiss: 15
        }
        this.notifRef.current.notificationAlert(options);
    }

    fetchUnitKerja = async() => {
        this.setState({isFetching:true, unitKerja: []})     
        const result = await apiUpin.get(END_POINT.UNIT_KERJA_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data;
                let option = [];
                array.map(d => {
                    let obj = {
                        value: d.wkuWrkcod,
                        label: d.nama
                    }
                    option.push(obj);
                });
                this.setState({unitKerja: option})
            }
            this.setState({isFetching:false}) 
        } 
    }

    fetchKelompokKasus = async() => {   
        this.setState({kelompokKasusList: []})     
        const result = await apiUpin.get(END_POINT.KELOMPOK_KASUS_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data.content;
                let optionKelompok = [];
                array.map(d => {
                    let obj = {
                        value: d.idKelompokKasus,
                        label: d.ppYangMengatur
                    }
                    optionKelompok.push(obj);
                });
                this.setState({kelompokKasusList: optionKelompok})
            }
        }
    }

    fetchStatusProses = async() => {   
        this.setState({ statusProsesList: []})     
        const result = await apiUpin.get(END_POINT.STATUS_PROSES_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data.content;
                let option = [];
                array.map(d => {
                    let obj = {
                        value: d.idStatusProses,
                        label: d.statusProses
                    }
                    option.push(obj);
                });
                this.setState({statusProsesList: option})
            }
        }
    }

    fetchKasusById = async(idKasus) => {
        const result = await apiUpin.get(END_POINT.API_KASUS + '/' + idKasus);
        if(result) {           
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    kasusModel: result.data,
                    validateNomor: false,
                    validateTanggal: false,
                    validateAsal: false,
                    validateKelompok: false,
                    validateStatus: false
                })
            } 
        } 
        
    }

    handleChangeNomor = (e) => {
        const value = e.target.value;  
        this.setState({validateNomor: (value === null || value === '') })         
        this.setState(prev => ({kasusModel: {...prev.kasusModel, noSuratAduan: value} }))
    }
    handleChangeTanggalSurat = (date) => { 
        this.setState({validateTanggal: (date === null || date === '') }) 
        this.setState(prevState => ({ kasusModel: {...prevState.kasusModel,tanggalSuratAduan: date} }))
    }
    handleChangeAsal = (e) => {
        const value = e.value;
        this.setState({validateAsal: (value === null || value === '') }) 
        this.setState(prevState => ({ kasusModel: {...prevState.kasusModel,unitKerja: value, asal: e.label} }))
    }
    handleChangeIsi(value) {
        this.setState(prev => ({kasusModel: {...prev.kasusModel, isiSurat: value} }))
    }
    handleChangeKeterangan(value) {
        this.setState(prev => ({kasusModel: {...prev.kasusModel, keterangan: value} }))
    }
    handleChangeKelompok = (e) => {
        const value = e.value;
        this.setState({validateKelompok: (value === null || value === '') }) 
        this.setState(prevState => ({ 
            kasusModel: {...prevState.kasusModel, kelompokKasus: {idKelompokKasus: value, ppYangMengatur: e.label}} }
        ))
    }
    handleChangeStatus = (e) => {
        const value = e.value;
        this.setState({validateStatus: (value === null || value === '') }) 
        this.setState(prevState => ({ 
            kasusModel: {...prevState.kasusModel, statusProses: {idStatusProses: value, statusProses: e.label}} }
        ))
    }

    validateSubmit = () => {
        const model = this.state.kasusModel;
        if(model.noSuratAduan === null || model.noSuratAduan === '') {
            this.setState({validateNomor: true})            
        }
        if(model.tanggalSuratAduan === null || model.tanggalSuratAduan === '') {
            this.setState({validateTanggal: true})
        }
        if(model.asal === null || model.asal === '') {
            this.setState({validateAsal: true})
        }
        if(model.kelompokKasus.idKelompokKasus === null || model.kelompokKasus.idKelompokKasus === '') {
            this.setState({validateKelompok: true})
        }
        if(model.statusProses.idStatusProses === null || model.statusProses.idStatusProses === '') {
            this.setState({validateStatus: true})
        }


        if(!this.state.validateNomor &&
            model.tanggalSuratAduan !== null &&
            !this.state.validateAsal &&
            !this.state.validateKelompok &&
            !this.state.validateStatus 
        ){
            this.setState({setShowModal: true})
        } 
    }

    onSubmit = async() => {
        this.setState({isFetching:true, setShowModal: false})
        let {kasusModel} = this.state;
        const result = await apiUpin.put(END_POINT.API_KASUS, kasusModel = {
            ...kasusModel, createdBy: this.props.account.nip
        });
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const data = JSON.parse(result.data.data)
                history.push({
                    pathname: '/ajuankasus/personal/' + data.idKasus
                })  
            } else {
                this.notify(result.message)
                this.setState({isFetching:false}) 
            }               
        }
    }

    render() {
        const kasusModel = this.state.kasusModel;
        let dateValue = moment(kasusModel.tanggalSuratAduan === null ? new Date() : new Date(kasusModel.tanggalSuratAduan)).format('YYYY-MM-DD')
        
        return(            
            <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Pengajuan Kasus"} />
              <div className="content">
                <Container fluid>
                    <Card>
                        <Card.Body>
                        <Form>
                            
                            <Row>                            
                                <Col md="4">
                                    <Form.Group>
                                        <label>Nomor Surat <span className='text-danger'>*</span></label>                                    
                                        <Form.Control ref={(input) => { this.inputNomor = input; }} 
                                        defaultValue={kasusModel.noSuratAduan}
                                        placeholder="Nomor surat"
                                        type="text" onChange={this.handleChangeNomor}
                                        ></Form.Control>
                                        {this.state.validateNomor && <div className="invalid-feedback d-block">
                                            Nomor Surat wajib diisi...
                                        </div>}                                    
                                    </Form.Group>
                                </Col>
                                <Col md="4">
                                <Form.Group> 
                                    <label>Tanggal Surat <span className='text-danger'>*</span></label>
                                    <Form.Control
                                    ref={(input) => { this.inputTanggal = input; }} 
                                    value={dateValue}
                                    placeholder="tanggal surat"
                                    type="date" onChange={e => this.handleChangeTanggalSurat(e.target.value)}
                                    ></Form.Control>
                                    {this.state.validateTanggal && <div className="invalid-feedback d-block">
                                    Tanggal Surat wajib diisi...
                                    </div>}    
                                </Form.Group>
                                </Col>
                            </Row>
                            <Col md="8">
                                <Form.Group>
                                    <label>Asal Surat <span className='text-danger'>*</span></label>                                
                                    <Select 
                                    options={this.state.unitKerja}
                                    value = {this.state.unitKerja.filter(option => 
                                        option.value === kasusModel.unitKerja)
                                    }
                                    onChange={e => this.handleChangeAsal(e)} />
                                    {this.state.validateAsal && <div className="invalid-feedback d-block">
                                    Asal Surat wajib diisi...
                                    </div>} 
                                </Form.Group>
                            </Col>

                            <Col md="8">
                                <Form.Group>
                                    <label>Isi Surat</label>
                                    <Form.Control
                                    defaultValue={kasusModel.isiSurat}
                                    onChange={e => this.handleChangeIsi(e.target.value)}
                                    as="textarea" rows={2}
                                    ></Form.Control>
                                </Form.Group>
                            </Col>

                            <Col md="8">
                                <Form.Group>
                                    <label>keterangan</label>
                                    <Form.Control
                                    defaultValue={kasusModel.keterangan}
                                    onChange={e => this.handleChangeKeterangan(e.target.value)}
                                    as="textarea" rows={2}
                                    ></Form.Control>
                                </Form.Group>
                            </Col>

                            <Col md="8">
                                <Form.Group as={Row}>
                                    <Col sm="4">
                                        <label>Kelompok Kasus <span className='text-danger'>*</span></label>
                                    </Col>                                
                                    <Col sm="8">
                                        <Select 
                                        options={this.state.kelompokKasusList} 
                                        value = {this.state.kelompokKasusList.filter(option => 
                                            option.value === kasusModel.kelompokKasus.idKelompokKasus)
                                        }
                                        onChange={e => this.handleChangeKelompok(e)}
                                        />
                                        {this.state.validateKelompok && <div className="invalid-feedback d-block">
                                        Kelompok Kasus Surat wajib diisi...
                                        </div>} 
                                    </Col>                                
                                </Form.Group>
                            </Col>

                            <Col md="8">
                                <Form.Group as={Row}>
                                    <Col sm="4">
                                        <label>Status Proses <span className='text-danger'>*</span></label>
                                    </Col>                                
                                    <Col sm="8">
                                        <Select 
                                        options={this.state.statusProsesList} 
                                        value = {this.state.statusProsesList.filter(option => 
                                            option.value === kasusModel.statusProses.idStatusProses)
                                        }                                        
                                        onChange={e => this.handleChangeStatus(e)}
                                        />
                                        {this.state.validateStatus && <div className="invalid-feedback d-block">
                                        Status Proses wajib diisi...
                                        </div>} 
                                    </Col>                                
                                </Form.Group>
                            </Col>
                            <Col md="8" className="text-center">
                                <Button variant="primary" size="lg" onClick={this.validateSubmit}>Proses</Button>
                            </Col>
                        </Form>
                        </Card.Body>
                    </Card>

                    {/* Mini Modal */}
                    <Modal
                    className="modal-mini modal-primary"
                    show={this.state.setShowModal}
                    onHide={() => this.setState({setShowModal:false}) }
                    >
                    <Modal.Header className="justify-content-center">
                        <div className="modal-profile">
                        <i className="nc-icon nc-bulb-63"></i>
                        </div>
                    </Modal.Header>
                    <Modal.Body className="text-center">
                        <p>Apakah anda yakin ? </p>
                    </Modal.Body>
                    <div className="modal-footer">
                        <Button
                        className="btn-simple"
                        type="button"
                        variant="danger"
                        onClick={() => this.setState({setShowModal:false}) }
                        >
                        Tidak
                        </Button>
                        <Button
                        className="btn-simple"
                        type="button"
                        variant="success"
                        onClick={ this.onSubmit }
                        >
                        Ya
                        </Button>
                    </div>
                    </Modal>
                    {/* End Modal */}
                </Container>
              </div>
              <Footer />
              <LoaderModal isFetching={this.state.isFetching} />
              <NotificationAlert ref={this.notifRef} zIndex={9999} />
            </div>
          </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(AjuanKasus);