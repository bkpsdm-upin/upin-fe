import React from 'react';
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import PaginationData from '../../entity/PaginationData';
import PaginationTable from '../../utils/PaginationTable';
import DateFormater from 'components/utils/DateFormater';
import {
    Card,
    Table,
    Container,
    Row,
    Col,
    Dropdown
  } from "react-bootstrap";

class KasusList extends React.Component{
    constructor(props) {
        super(props);
        this.state = { 
            isFetching: false,
            dataList: [],
            paginationData: PaginationData
        }
    }

    async componentDidMount() { 
        await this.props.cekCookies();    
        if(!__.isEmpty(this.props.account)) {
            this.fetchKasus();
            
        }
    }

    fetchKasus = async() => {   
        console.log('paging: ', this.state.paginationData)
        this.setState({isFetching:true, dataList: []})     
        const result = await apiUpin.get(END_POINT.API_KASUS, {
            params: { page: this.state.paginationData.page, 
                    size: this.state.paginationData.size, }
        });
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                this.setState({
                    dataList: result.data.content,
                    paginationData: {...this.state.paginationData, 
                        page: result.data.page,
                        size: result.data.size,
                        totalPages: result.data.totalPages,
                        totalElements: result.data.totalElements,
                        last: result.data.last
                    },
                    isFetching: false
                });
                   
            }
        }
    }

    handleChangePage = async(newPage) => {
        this.state.paginationData.page = newPage;
        this.fetchKasus();
    }

    handleChangeRowsPerPage = (size) => {
        this.state.paginationData.size = size;
        // this.state.page = 0;
        // this.state.size = size;
        this.fetchKasus();
    }

    render() {
        return(
          <>
            <LoaderModal isFetching={this.state.isFetching} /> 
            <Container fluid>
                <Row>
                <Col md="12">
                    <Card className="strpied-tabled-with-hover">
                    <Card.Body className="table-full-width table-responsive px-0">
                        <Table className="table-hover small">
                        <thead>
                            <tr>
                            <th className="border-0">Asal</th>
                            <th className="border-0">Uraian Kasus</th>                            
                            <th className="border-0">Tanggal Aduan</th>
                            <th className="border-0">Kelompok</th>
                            <th className="border-0">Status</th>
                            <th className="border-0">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.dataList.map((data, idx) => 
                                <tr>
                                    <td>{data.asal}</td>
                                    <td>{data.isiSurat}</td>
                                    <td><DateFormater value={data.tanggalSuratAduan} /></td>
                                    <td>{data.kelompokKasus == null ? '-' : data.kelompokKasus.ppYangMengatur}</td>
                                    <td>{data.statusProses == null ? '-' : data.statusProses.statusProses}</td>                                    
                                    <td>
                                        <Dropdown size="small">
                                            <Dropdown.Toggle variant="info" id="dropdown-basic">
                                                Aksi
                                            </Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                <Dropdown.Item href="#/action-1">Buat Panggilan</Dropdown.Item>
                                                <Dropdown.Divider />
                                                <Dropdown.Item href="#/action-1">Putusan Hukdis</Dropdown.Item>
                                                <Dropdown.Divider />
                                                <Dropdown.Item href="#/action-2">Edit</Dropdown.Item>
                                                <Dropdown.Item href="#/action-3">Delete</Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                        </Table>
                    </Card.Body>
                    </Card>
                    
                    <PaginationTable 
                        paging={this.state.paginationData} 
                        handleChangePage={this.handleChangePage} 
                        handleChangeRowsPerPage={this.handleChangeRowsPerPage} />
                </Col>
                </Row>
            </Container>
          </>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(KasusList);