import React from "react";
import { connect } from 'react-redux';
import {cekCookies} from '../../../action';
import __ from 'lodash';
import history from '../../../history';
import LoaderModal from '../../Loader/LoaderModal';
import { END_POINT } from '../../../config';
import apiUpin from '../../../apis/apiUpin';
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "../../../components/Sidebar/Sidebar";
import Select from "react-select";
import { Container, Card, Form, Row, Col, Button, Modal } from "react-bootstrap";
import Hukdis from "../../entity/Hukdis";
import NotificationAlert from "react-notification-alert";

class Personal extends React.Component {
    constructor(props) {
        super(props);
        this.notifRef = React.createRef(null);
        this.state = { 
            isFetching: false,
            user: {},
            statusPersonal: [],
            isDisabled: false,
            dataHukdis: {...Hukdis, idKasus: this.props.match.params.id},
            validateStatus: true,
            validateNama: true,
            setShowModal: false
        }
    }

    async componentDidMount() { 
        await this.props.cekCookies();
        if(!__.isEmpty(this.props.account)) {
            this.fetchStatusPersonal();
        }
        window.addEventListener("focus", this.onFocus)
    }

    componentWillUnmount() {        
        window.removeEventListener("focus", this.onFocus)        
        this.setState = (state,callback)=>{
            return;
        };
    }

    onFocus = () => {
        // if(window.confirm('Apakah anda yakin ingin keluar dari halaman ini ?')) {
        //     this.handleBack();
        // }
    }

    fetchStatusPersonal = async() => {
        this.setState({ statusPersonal: []})
        const result = await apiUpin.get(END_POINT.STATUS_PERSONAL_LIST);
        if(result) {
            if(result.status === END_POINT.SUCCESS_RESPONSE_CODE) {
                const array = result.data.content;
                let option = [];
                array.map(d => {
                    let obj = {
                        value: d.idStatus,
                        label: d.status
                    }
                    option.push(obj);
                });
                this.setState({statusPersonal: option})
            }
        }
    }

    getUserByNip = async(nip) => {
        const result = await apiUpin.get(END_POINT.USER_BY_NIP + nip).catch(error => console.log(error));
        if(result){
            if(result.data.success === true) {
                const data = JSON.parse(result.data.data);
                this.setState({
                    isDisabled: true,
                    validateNama: false
                })
                this.setState(prevState => ({ 
                    dataHukdis: {...prevState.dataHukdis, 
                        nipLama: data.id,
                        nip: data.nipBaru,
                        nama: data.nama,
                        glrDepan: data.pnsFtitle,
                        glrBelakang: data.pnsRtitle,
                        unitKerja: {...prevState.dataHukdis.unitKerja, wkuWrkcod: data.unitKerja.wkuWrkcod, nama: data.unitKerja.nama},
                        jabatan: {...prevState.dataHukdis.jabatan, kdJabatan: data.jabatan.kdJabatan, jabatan: data.jabatan.jabatan},
                        golru: {...prevState.dataHukdis.golru, id: data.golongan.id, golongan: data.golongan.golongan, pangkat: data.golongan.pangkat},
                        atasanLangsung: 'Kepala ' + data.unitKerja.nama
                    } 
                }))
            }
        }
    }

    handleChangeNip = _.debounce((value) => {
        this.setState({ dataHukdis: {...Hukdis, idKasus: this.props.match.params.id}, 
            validateNama:true, 
            validateStatus: true,
            isDisabled: false })
        if(!(value === null || value === ""))
            this.getUserByNip(value);
    }, 700)

    handleKeyDown = (e) => {
        if (e.keyCode === 13) {
            this.getUserByNip(e.target.value);
        }
    }

    handleChangeStatus = (e) => {
        const value = e.value;
        this.setState({validateStatus: (value === null || value === '')}) 
        this.setState(prevState => ({ 
            dataHukdis: {...prevState.dataHukdis, statusPersonal: {...prevState.dataHukdis.statusPersonal, idStatus: value} } }
        ))
    }

    handleChangeNama = (value) => {
        this.setState({validateNama: (value === null || value === '') }) 
        this.setState(prevState => ({ 
            dataHukdis: {...prevState.dataHukdis, nama: value} }
        ))
    }

    handleChangeUker = (value) => {
        this.setState(prevState => ({ dataHukdis: {...prevState.dataHukdis, unitKerja: {...prevState.dataHukdis.unitKerja, wkuWrkcod: value}} } ))
    }

    handleChangeAlamat = (value) => {
        this.setState(prevState => ({ dataHukdis: {...prevState.dataHukdis, alamat: value} } ))
    }

    handleChangeKeterangan = (value) => {
        this.setState(prevState => ({ dataHukdis: {...prevState.dataHukdis, keterangan: value} } ))
    }

    handleChangeAtasan = (value) => {
        this.setState(prevState => ({ dataHukdis: {...prevState.dataHukdis, atasanLangsung: value} } ))
    }

    render() {
        const {dataHukdis, isDisabled, statusPersonal} = this.state;
        return(
          <div className="wrapper">
            <Sidebar user={this.props.account} />
            <div className="main-panel">
              <AdminNavbar title={"Pengajuan Kasus - Data Personal"}/>
              <div className="content">
                <Container fluid>
                    <Card>
                        <Card.Header>                        
                        <p className="card-category">
                        <span className="text-danger">*</span>Apabila <strong>BUKAN PNS</strong> isikan <i>nama, alamat, status personal, dan keterangan</i>
                        </p>
                        </Card.Header>
                        <Card.Body>
                        <Form>
                            <Row>                    
                                <Col md="6">
                                    <Form.Group>
                                        <label>Nip</label>  
                                        <Form.Control
                                        onChange={e => this.handleChangeNip(e.target.value)}
                                        onKeyDown={this.handleKeyDown}
                                        defaultValue={dataHukdis.nip|| ""}
                                        placeholder="input NIP"
                                        id="nip"
                                        type="number"
                                        >
                                    </Form.Control>                                         
                                    </Form.Group>
                                </Col>
                                <Col md="6">
                                    <Form.Group>
                                        <label>Nama <span className="text-danger">*</span></label>  
                                        <Form.Control
                                        disabled={isDisabled}
                                        value={dataHukdis.nama || ""}
                                        placeholder="input Nama"
                                        type="text" 
                                        onChange={e => this.handleChangeNama(e.target.value)}
                                        ></Form.Control> 
                                        {this.state.validateNama && <div className="invalid-feedback d-block">
                                        Nama wajib diisi...
                                        </div>}  
                                    </Form.Group>
                                </Col>   
                            </Row>
                            <Row>
                                <Col md="6">
                                    <Form.Group>
                                        <label>Jabatan</label>  
                                        <Form.Control disabled={true}
                                        defaultValue={dataHukdis.jabatan.jabatan || ''}
                                        as="textarea" rows={1} 
                                        ></Form.Control> 
                                    </Form.Group>
                                </Col>
                                <Col md="6">
                                    <Form.Group>
                                        <label>Unit Kerja</label>  
                                        <Form.Control disabled={true}
                                        value={dataHukdis.unitKerja.nama || ''}
                                        as="textarea" rows={1} 
                                        onChange={e => this.handleChangeUker(e.target.value)}
                                        ></Form.Control> 
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>                               
                                <Col md="6">
                                    <Form.Group>
                                        <label>Alamat</label>  
                                        <Form.Control
                                        value={dataHukdis.alamat}
                                        placeholder="input alamat"
                                        as="textarea" rows={2} 
                                        onChange={e => this.handleChangeAlamat(e.target.value)}
                                        ></Form.Control> 
                                    </Form.Group>
                                </Col>     
                                <Col md="6">
                                    <Form.Group>
                                        <label>Keterangan</label>  
                                        <Form.Control
                                        value={dataHukdis.keterangan}
                                        placeholder="input keterangan"
                                        as="textarea" rows={2} 
                                        onChange={e => this.handleChangeKeterangan(e.target.value)}
                                        ></Form.Control> 
                                    </Form.Group>
                                </Col>                           
                            </Row>
                            <Row>
                                <Col md="6">
                                    <Form.Group>
                                        <label>Status Personal <span className='text-danger'>*</span></label>    
                                        <Select 
                                        options={statusPersonal} 
                                        defaultValue={dataHukdis.statusPersonal}
                                        onChange={e => this.handleChangeStatus(e)}
                                        />
                                        {this.state.validateStatus && <div className="invalid-feedback d-block">
                                        Status Personal wajib diisi...
                                        </div>}                                                                      
                                    </Form.Group>
                                </Col>
                                <Col md="6">
                                    <Form.Group>
                                        <label>Atasan Langsung</label>  
                                        <Form.Control
                                        value={"Kepala " + (dataHukdis.unitKerja.nama == null ? "" : dataHukdis.unitKerja.nama)}
                                        placeholder="input atasan langsung"
                                        type="text" 
                                        onChange={e => this.handleChangeAtasan(e.target.value)}
                                        ></Form.Control> 
                                    </Form.Group>
                                </Col>
                            </Row>
                                                     
                            <Col md="12" className="text-center">  <br /></Col>
                            
                            <Row className="justify-content-center">
                                <Col md="4" className="text-left">
                                    <Button variant="danger" size="md" onClick={this.handleBack}>
                                        <i className="fa fa-arrow-left"></i> Kembali</Button>                                
                                </Col>  
                                <Col md="4" className="text-center">
                                    <Button variant="primary" size="md" onClick={this.modalConfirm}>Simpan</Button>                                
                                </Col> 
                                <Col md="4" className="text-right">
                                    <Button variant="secondary" size="md">
                                     Selanjutnya <i className="fa fa-arrow-right"></i></Button>                                
                                </Col> 
                            </Row> 
                        </Form>
                        </Card.Body>
                    </Card>                    
                </Container>
              </div>
              <Footer />
            </div>  
            <NotificationAlert ref={this.notifRef} zIndex={9999} />
            <LoaderModal isFetching={this.state.isFetching} />   
            {/* Mini Modal */}
            <Modal
                className="modal-mini modal-primary"
                show={this.state.setShowModal}
                onHide={() => this.setState({setShowModal:false}) }
                >
                <Modal.Header className="justify-content-center">
                    <div className="modal-profile">
                    <i className="nc-icon nc-bulb-63"></i>
                    </div>
                </Modal.Header>
                <Modal.Body className="text-center">
                    <p>Apakah anda yakin ? </p>
                </Modal.Body>
                <div className="modal-footer">
                    <Button
                    variant="danger"
                    onClick={() => this.setState({setShowModal:false}) }
                    >
                    Tidak
                    </Button>
                    <Button
                    variant="primary"
                    onClick={ this.handleSubmit }
                    >
                    Ya
                    </Button>
                </div>
            </Modal>
            {/* End Modal */}       
          </div>
        )
    }

    modalConfirm = () => {        
        const {dataHukdis, validateNama, validateStatus} = this.state;
        if(dataHukdis.nama === null || dataHukdis.nama === '') {
            this.setState({validateNama: true})            
        }
        if(dataHukdis.statusPersonal === null || dataHukdis.statusPersonal === '') {
            this.setState({validateStatus: true})
        }
        if(!validateStatus && !validateNama) {
            this.setState({setShowModal: true})
        }
    }

    handleSubmit = async() => {
        this.setState({isFetching:true, setShowModal: false})
        const {dataHukdis} = this.state;
        const result = await apiUpin.put(END_POINT.HUKDIS_LIST,dataHukdis);
        if(result) {
            if(result.data.success) {
                this.setState({ 
                    dataHukdis: {...Hukdis, idKasus: this.props.match.params.id, statusPersonal: {idStatus: null, status: null}},
                    isDisabled: false,
                    validateNama: true}) 
                    document.getElementById("nip").value = "";
                this.notify("primary", "Data Berhasil Disimpan...");
            } else {
                this.notify("danger", result.data.message)                
            }        
            this.setState({isFetching:false})        
        }
    }

    notify = (tipe, message) => {
        var options = {};
        options = {
            place: 'tc',
            message: (
                <div>{message}</div>
            ),
            type: tipe,
            icon: "nc-icon nc-bell-55",
            autoDismiss: 10
        }
        this.notifRef.current.notificationAlert(options);
    }

    handleBack = () => {
        const id = this.props.match.params.id;
        history.push({
            pathname: '/ajuankasus',
            state: {idKasus: id} 
        })
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account        
    };
}
export default connect(mapStateToProps, {cekCookies})(Personal);
