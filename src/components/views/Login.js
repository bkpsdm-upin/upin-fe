import React from 'react';
import {reduxForm} from 'redux-form';
import { connect } from 'react-redux';
import {signIn, cekCookies} from '../../action';
import LoaderModal from '../Loader/LoaderModal';
import 'assets/css/login/main.css'

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isFetching: false,
            username: '',
            password: ''
        }
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    async componentDidMount(){        
        await this.props.cekCookies('login');
    }

    renderError({error, touched}){
        if(touched && error){
            return(
                <small>{error}</small>
            )
        }
    }

    handleChangeUsername(event) {
        this.setState({username: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    
    submitLogin = async () => {         
        let formData = {
            username: this.state.username,
            password: this.state.password
        }      
        
        this.setState({isFetching: true});
        await this.props.signIn(formData ); 
        this.setState({isFetching: false});          
    }

    renderAlert = () => {             
        if(this.props.fetchError){
            if(this.props.fetchError.type === 'error'){
                return(
                    <p className="text-danger">
                        {this.props.fetchError.message}
                    </p>
                )
            }
        }
    }


    render(){
        return (
          <div className="limiter">         
            <div className="container-login100">
                <div className="wrap-login100">                    
                    <div className="login100-pic js-tilt" data-tilt>
                        <img src={require("assets/css/login/img-01.png")} alt=""></img>
                    </div>
    
                    <form className="login100-form" onSubmit={this.props.handleSubmit(this.submitLogin)}>
                        <span className="login100-form-title">
                            Login SiUpin
                        </span>
    
                        <div className="wrap-input100 validate-input" data-validate = "Valid username is required">                            
                            <input className="input100" type="text" name="username" placeholder="Username" 
                            value={this.state.username} onChange={this.handleChangeUsername} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <i className="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>
    
                        <div className="wrap-input100 validate-input" data-validate = "Password is required">
                            <input className="input100" type="password" name="pass" placeholder="Password"
                                value={this.state.password} onChange={this.handleChangePassword} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <i className="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        
                        <div className="container-login100-form-btn">
                            <button className="login100-form-btn">
                                Login
                            </button>
                        </div>
    
                        <div className="text-center p-t-12">
                            <span className="txt1">
                                Forgot &nbsp;
                            </span>
                            <a className="txt2" href="#">
                                Password?
                            </a>
                        </div>
                        {/* {this.renderAlert} */}

                        
    
                        {/* <div className="text-center p-t-136">
                            <div className="txt3">                               
                                <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                
                            </div>
                        </div> */}
                        <div className="typography-line text-center">
                            {this.renderAlert()}
                        </div>
                    </form>
                </div>
            </div>
            
            <LoaderModal isFetching={this.state.isFetching}/>
          </div>
        );
    }
}

const validate = (formValues) => {
    const errors = {};
    if(!formValues.username){
        errors.username = "Anda harus memasukkan username";
    }
    if(!formValues.password){
        errors.password = "Anda harus memasukkan password";
    }
    return errors;
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        fetchError: state.error
    }
}

const formWrapped = reduxForm({
    form: 'login',
    validate
})(Login);

export default connect(mapStateToProps, {signIn, cekCookies})(formWrapped);