import React from 'react';
class LoaderModal extends React.Component{
    
    renderCondition(){
        if(this.props.isFetching){
            return(
                <div className="modal fade show" id="modalLoader" style={{display: 'block', backgroundColor: 'rgba(0,0,0,0.8)'}} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{border: 'none', backgroundColor: 'rgba(0,0,0,0)'}}>
                        <div className="modal-body text-center" style={{minHeight: '80vh', padding: '6rem'}}>
                            <img alt="loading" src={require("assets/img/loading-bubbles.svg").default} width="80"/>  
                        </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render(){
        return(
            <div>
            {this.renderCondition()}
            </div>
        )
    }
}

export default LoaderModal;