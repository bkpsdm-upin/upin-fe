import React from 'react';
import {Router, Route} from 'react-router-dom'
import history from '../history';
import {connect} from 'react-redux';
import Login from './views/Login';
import AdminLayout from "layouts/Admin.js";
import Personal from './views/kasus/Personal';
import Dashboard from './views/Dashboard';
import AjuanKasus from './views/kasus/AjuanKasus';
import VerifikasiKasus from './views/kasus/VerifikasiKasus';
import VerifikasiKasusDetail from './views/kasus/VerifikasiKasusDetail';
import PutusanHukdis from './views/hukdis/PutusanHukdis';
import DetailPutusan from './views/hukdis/DetailPutusan';
import SuratPanggilan from './views/panggilan/SuratPanggilan';

class App extends React.Component{
    render() {
        return(
            <div>
                <Router history={history}>
                    <Route path="/" exact component={Login} />
                    <Route path="/login" exact component={Login} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/ajuankasus" exact component={AjuanKasus} />
                    <Route path="/ajuankasus/personal/:id" exact component={Personal} />
                    <Route path="/verifikasikasus" exact component={VerifikasiKasus} />
                    <Route path="/verifikasikasus/detail" exact component={VerifikasiKasusDetail} />
                    <Route path="/hukdis" exact component={PutusanHukdis} />
                    <Route path="/hukdis/detail/:id" exact component={DetailPutusan} />
                    <Route path="/panggilan" exact component={SuratPanggilan} />
                </Router> 
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        account: state.account
    }
}
export default connect(mapStateToProps)(App);