const KasusModel = {
    idKasus: null,
    asal: null,
    unitKerja: null,
    alamat: null,
    noSuratAduan: null,
    tanggalSuratAduan: new Date(),
    isiSurat: null,
    keterangan: null,
    kelompokKasus: {idKelompokKasus: null, ppYangMengatur: null},
    statusProses: {idStatusProses: null, statusProses: null},
    statusVerifikasi: null,
    createdAt: new Date(),
    createdBy: null,
    verifiedAt: null,
    verifiedBy: null
}

export default KasusModel;