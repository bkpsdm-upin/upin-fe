const PaginationData = {
    page: 0,
    size: 10,
    last: false,
    totalElements: 0,
    totalPages: 1   
}

export default PaginationData;