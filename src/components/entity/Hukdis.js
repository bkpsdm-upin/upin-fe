const Hukdis = {
    idHukdis: null,
    idKasus: null,
    nipLama: null,
    nip: null,
    nama: null,
    alamat: "",
    glrDepan: null,
    glrBelakang: null,
    golru: {id: null, golongan: null, pangkat: null},
    unitKerja: {wkuWrkcod: null, nama: null},
    jabatan: {kdJabatan: null, jabatan: null},
    keterangan: "",
    statusPersonal: {idStatus: null, status: null},
    atasanLangsung: null,
    pejabatYgBerwenang: null,
    noSkPutusan: null,
    tglSkPutusan: null,
    putusanHukdis: null,
    lamaHukdisTh: null,
    lamaHukdisBl: null,
    berlakuDari: null,
    berlakuSampai: null,
    tppBerlakuSampai: null
}

export default Hukdis;