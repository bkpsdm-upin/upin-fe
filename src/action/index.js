import history from "../history";
import { API_LOGIN } from "../config";
import Login from "../apis/Login";
import {
    SIGN_IN,
    FETCH_ERROR,
    SIGN_OUT,
    CEK_COOKIES
} from './types';

const errorObj = {type: '', message: ''};
export const signIn = (userInfo) => dispatch => {
    fetch('http://103.108.187.203/apis/login', {
        method: "POST",
        body: JSON.stringify(userInfo)
    })
    .then(res => res.json())
    .then(data => {        
        if(data.status === false) {
            errorObj.type = 'error';
            errorObj.message = data.message;
            dispatch({
                type: FETCH_ERROR,
                payload: errorObj
            })
            history.push('/login');
        } 
        else {
            let user = {
                nama: data.data.nama,
                nip: data.data.nip,
                role: data.data.role_id,
                token: data.access_token
            }
            localStorage.setItem("userUpin", JSON.stringify(user));
            dispatch({
                type: SIGN_IN,
                payload: data
            })
            errorObj.type = 'success';
            errorObj.message = 'success';
            dispatch({
                type: FETCH_ERROR,
                payload: errorObj
            })
            history.push('dashboard');
        }
    })
}

export const signOut = () => async (dispatch) => {
    localStorage.removeItem("userUpin")  
    
    errorObj.type = 'success';
    errorObj.message = 'User Logged Out...!!';
    dispatch({
        type: SIGN_OUT,
        payload: null
    })
    history.push('/login');    
}

export const cekCookies = (fromPage = null) => async (dispatch) => {    
    const user = JSON.parse(localStorage.getItem("userUpin"));
    if(user === null) {
        errorObj.type = 'error';
        errorObj.message = 'Not authorized';
        dispatch({
            type: FETCH_ERROR,
            payload: errorObj
        }) 
        if(fromPage !== 'login'){
            history.push('/login');
        }
    } else {
        dispatch({
            type: CEK_COOKIES,
            payload: user
        })
        errorObj.type = 'success';
        errorObj.message = "success";
        dispatch({
            type: FETCH_ERROR,
            payload: errorObj
        })

        
        if(fromPage === 'login'){
            history.push('/dashboard');
        }
    }   
}